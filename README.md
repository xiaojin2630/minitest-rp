# Minitest-rp (2018/06-- 至今)

#### 介绍
基于MiniTest，使用Ruby开发的自动化脚本批量调用、执行、数据汇总并生成美观、清晰的HTML报告的工具。

#### 它能做什么？
当你用ruby写了很多的自动化测试脚本，脚本是按模块划分，分布在不同的文件夹下，便于结构清晰和管理。但你如何运行这些脚本呢？ 
一一用ruby */*/tc_*.rb来运行吗？ 还是用shell写个批量脚本来执行？如何指定按跑哪个或几个模块的testcase? 如何对跑完后的结果，有个汇总，生成报告呢？

上面这些问题minitest-rp可以做到。它就是做这个事的。 

#### 为什么写这个工具
MiniTest下也有生成html的gem，但太过简单，不够直观，数据表现不丰富。批量调用testcase还要学习rake；但又找不到第三方较好的gem，所以自己花了些时间，开发这个工具，目前工具在部门内部一直在使用。

#### 基于特性
● ⽀持对所有模块脚本进⾏汇总显⽰；

● ⽀持批量跑⾃动化脚本；

● 支持命令行显示用例执行的进度；

● 支持仅执行指定的某个或某几个模块下的testcase（模块可以是1级或2级或3级模块）

● ⽀持测试⽤例按全部、通过、失败、错误进⾏过滤显⽰查看；

● ⽀持对每个test_⽅法添加 备注信息；

● ⽀持模块和负责⼈关联，⽅便查看模块⾃动化⽤例的归属；

● 支持最终测试数据的汇总和统计；

#### 感谢人
   同事许健（qq 810599089），擅长Java、前后端开发。在此系统中的开发过程，他主要负责前端的开发工作。本人负责除前端外的后端及底层工作。

#### 演示截图、
1 从命令行show模块清单到run testcase再到查看html报告这个过程


查看gif动画: [show和run_test.html.gif](https://gitee.com/xiaojin2630/minitest-rp/blob/master/show%E5%92%8Crun_test.html.gif)

![图表](./show和run_test.html.gif)

2 html页面超长截图
![图表](./页面长截图.png)

3.  整体HTML动画演示

查看gif动画: [all.gif](https://gitee.com/xiaojin2630/minitest-rp/blob/master/all.gif)
![图表](./all.gif)

查看gif动画: [两种视图](https://gitee.com/xiaojin2630/minitest-rp/blob/master/%E6%B5%8B%E8%AF%95%E7%BB%93%E6%9E%9C%E4%B8%A4%E7%A7%8D%E8%A7%86%E5%9B%BE.gif)

![两种视图](./测试结果两种视图.gif)

4.  图表
![图表](./1-图表.png)

5.  显示子模块汇总
![图表](./2-图表_显示子模块汇总.png)

6.  表格详细数据展示及导航
![图表](./3-表格详细数据展示及导航.png)

7.  按模块层级关系查看testcase测试结果
![图表](./4-按模块层级关系查看testcase测试结果.png)

8.  tc_file及testcase结果列表
![图表](./5-tc_file及testcase结果列表.png)

9.  命令行显示用例执行进度

查看gif动画: [用例执行进度.gif](https://gitee.com/xiaojin2630/minitest-rp/blob/master/6-命令行显示用例执行进度.gif)